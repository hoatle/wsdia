﻿require([
  'controller',
  'common/jquery.address-1.4.min',
  'common/supersized.3.2.6',
  'common/supersized.gallery',
  'common/jquery.hoverIntent.minified',
  'common/jquery.easing.1.3',
  'common/jquery.mousewheel',
  'common/jquery.gridnav',
  'common/functions',

  'pages/home',
  'pages/bulletin',
  'pages/work',
  'pages/gallery',
  'pages/project',
  'pages/information',
  'pages/contact'
],
   function (controller) {

     var workGallery = /work\/[A-z0-9]+\/gallery\/(\d+)/i,
        workFilterRegex = /work\/filter\/(\d+)/i,
        workRegex = /work/i,
        bulletinRegex = /bulletin/i,
        informationRegex = /information/i,
        contactRegex = /contact/i;

     var currentGalleryUrl;

     var firstRouted = false;

     var homePageLoaded = false,
         workPageLoaded = false,
         bulletinPageLoaded = false,
         informationPageLoaded = false,
         contactPageLoaded = false;


     var currentPage, loadedResources = arguments, pages = {};

     //controller mapping

     controller.map(workGallery, function(value) {
       $('#page-loader').show();
       currentGalleryUrl = value;
       normalizeGalleryUrl();
       //when currentGalleryUrl = '#/work/nike/gallery/2' => #/work/nike/gallery/1

       //to have list of gallery items for keyboard navigation
       if (!workPageLoaded) {
         workPageLoaded = true;
         display({name:'work'});
       }

       var patterns = value.split('/');
       var clientName = patterns[2], startSlide = patterns[4];
       display({name:'gallery', clientName:clientName, startSlide:startSlide});
       bindKeyboardNavigation(true);
     });

     controller.map(workFilterRegex, function (value) {
       var filterCategory = value.substring(value.lastIndexOf('/filter/') + 1 + 7, value.length);
       //remove the last trailing forward slash if any
       filterCategory = removeLastTrailingForwardSlash(filterCategory);

       //need to validate the filter category here to avoid XSS
       //TODO this is not perfect yet
       if (_.isNumber(parseInt(filterCategory))) {
         //$.log('display filter work');
         display({name:'work', func:'filter', category:filterCategory});
       } else {
         display({name:'work'});
       }
     });



     controller.map(workRegex, function (value) {
       if (!workPageLoaded) {
         $('#page-loader').show();
       }
       workPageLoaded = true;
       display({name:'work'});
     });

     controller.map(bulletinRegex, function (value) {
       if (!bulletinPageLoaded) {
         $('#page-loader').show();
       }
       bulletinPageLoaded = true;
       display({name:'bulletin'});
     });

     controller.map(informationRegex, function (value) {
       if (!informationPageLoaded) {
         $('#page-loader').show();
       }
       informationPageLoaded = true;
       display({name:'information'});
     });

     controller.map(contactRegex, function (value) {
       if (!contactPageLoaded) {
         $('#page-loader').show();
       }
       contactPageLoaded = true;
       display({name:'contact'});
     });

     //register hash address change event
     $.address.externalChange(function () {
       $.log('externalChange');

       if (currentGalleryUrl) {
         var currentClientName = getClientName(currentGalleryUrl);
         var nextClientName = getClientName($.address.value());
         if (currentClientName == nextClientName) {
           var targetSlideNumber = getTargetSlideNumber($.address.value());
           galTheme.handleGoTo(targetSlideNumber);
         } else {
           currentGalleryUrl = null;
           route();
         }
       } else {
         currentGalleryUrl = null;
         route();
       }
     });

     //routes only when changing between 2 clientName galleries
     $.address.internalChange(function() {
       $.log('internalChange');

       var currentClientName = getClientName(currentGalleryUrl);
       var nextClientName = getClientName($.address.value());
       if (currentClientName !== nextClientName) {
         route();
       }
     });


     function route() {
       firstRouted = true;
       controller.route($.address.value(), function (hashValue) {
         //when no matching router, display the home page.
         if (!homePageLoaded) {
           $('#page-loader').show();
         }
         homePageLoaded = true;
         display({name:'home'});
         bindKeyboardNavigation(false);
       });
     }

     function firstRoute() {
      if (!firstRouted) {
        route();
      }
     }

     //work around
     //timeout for chrome to fire route event change
     setTimeout(firstRoute, 100);


     /**
      * Normalizes the gallery url to make sure:
      *
      * #/work/nike/gallery/2 => #/work/nike/gallery/1
      *
      */
     function normalizeGalleryUrl() {
       currentGalleryUrl = '#' + currentGalleryUrl;
       var arr = currentGalleryUrl.split('/');
       arr.pop();
       arr.push('1');
       currentGalleryUrl = arr.join('/');
       $.log('currentGalleryUrl', currentGalleryUrl);
     }

     /**
      * Gets the client name by the hash value.
      *
      * @param hashValue
      */
     function getClientName(hashValue) {
       var arr = hashValue.split('/');
       return arr[2];
     }

     /**
      * Gets the target slide number.
      *
      * @param hashValue
      */
     function getTargetSlideNumber(hashValue) {
       var arr = hashValue.split('/');
       return arr[4];
     }

     /**
      * Goes to the previous gallery url basing on currentGalleryUrl.
      *
      */
     function goToPreviousGalleryUrl() {
       //

       var selector = '[href="'+ currentGalleryUrl +'"]';
       var currentA = $(selector);
       $.log('currentA', currentA);
       var prevLi = (currentA).parent().prev();
       $.log('prevLi', prevLi);
       if (prevLi.length > 0) {
         var prevA = prevLi.find('a')[0];
         var prevHashValue = $(prevA).attr('href');
         $.log('prevHashValue', prevHashValue);
         $.address.value(prevHashValue.replace('#/', ''));
       }

     }

     /**
      * Goes to the next gallery url basing on currentGalleryUrl.
      */
     function goToNextGalleryUrl() {
       var selector = '[href="'+ currentGalleryUrl +'"]';
       var currentA = $(selector);
       $.log('currentA', currentA);
       var nextLi = currentA.parent().next();
       $.log('nextLi', nextLi);
       if (nextLi.length > 0) {
         var nextA = nextLi.find('a')[0];
         var nextHashValue = $(nextA).attr('href');
         $.address.value(nextHashValue.replace('#/', ''));
       }
     }

     /**
      * Removes the last trailing forward slash if any.
      * abc/ => abc
      * abc => abc
      * @param str the string
      */
     function removeLastTrailingForwardSlash(str) {
       if (str.lastIndexOf('/') === (str.length - 1)) {
         return str.substring(0, str.lastIndexOf('/'));
       }
       return str;
     }

     function display(pageParams, finishCallback) {
       var prevPage = currentPage;
       currentPage = getPage(pageParams);
       if (currentPage != prevPage) {
         currentPage.container.show();
         currentPage.beforeShow(pageParams);

         var forward = !prevPage || (currentPage.index > prevPage.index);

         if (prevPage) {
           if ($.isFunction(prevPage.beforeHide)) {
             prevPage.beforeHide();
           }
           prevPage.container.animate(
              { top:(forward ? -1 : 1) * $(window).innerHeight() },
              1000,
              'easeInOutExpo',
              function () {
                if (this == prevPage.container.filter('.page-container')[0]) {
                  prevPage.container.hide();
                  if ($.isFunction(prevPage.afterHide))
                    prevPage.afterHide();
                }
              }
           );
         }

         currentPage.container.css('top', ((forward ? 1 : -1) * $(window).innerHeight()) + 'px');
         $('#page-loader').fadeOut(500, function() {
           currentPage.container.animate(
              { top:0 },
              1000,
              'easeInOutExpo',
              function() {
                //in case loads directly #/work for example
                $('#supersized-loader').hide();
                if ($.isFunction(currentPage.afterShow)) {
                  currentPage.afterShow();
                }
                if ($.isFunction(finishCallback)) {
                  finishCallback();
                }
              }
           );
         });
       } else { //currentPage == prevPage
         currentPage.beforeShow(pageParams);
         $('#page-loader').fadeOut(500);
       }
     }

     function getPage(pageParams) {

       var pageName = pageParams ? pageParams.name : 'home';

       if (pages[pageName]) {
         return pages[pageName];
       }

       for (var i = 0; i < loadedResources.length; i++) {
         var resource = loadedResources[i];
         if (resource && resource.name == pageName) {
           var container = $('<div>')
              .addClass(pageName + '-page-container')
              .addClass('page-container')
              .appendTo($('.pages'));

           pages[pageName] = new resource.constructor(container);
           pages[pageName].index = $('.pages .page-container').length;
           return pages[pageName];
         }
       }

       return getPage({ name:'home' });
     }

     $(window).bind('resize', function () {
       $('.pages').css({ width:$(window).innerWidth(), height:$(window).innerHeight() })
     });

     // Keyboard Navigation
     function bindKeyboardNavigation(isGallery) {

       $.log('bindKeyboardNavigation', isGallery);

       //reset
       $(document).off('keyup');

       $(document).keyup(function (event) {
         if (isGallery) {
          if (galVars.in_animation) return false;// Abort if currently animating
         } else {
           if (vars.in_animation) return false;// Abort if currently animating
         }

         // Left Arrow or Down Arrow
         if ((event.keyCode == 37) || (event.keyCode == 40)) {
           if (isGallery) {
            clearInterval(galVars.slideshow_interval);// Stop slideshow, prevent buildup
            if (event.keyCode == 37) {
              galTheme.prevSlide();
            } else {
              //down arrow => previous gallery
              goToPreviousGalleryUrl();

            }
           } else {
             clearInterval(vars.slideshow_interval);// Stop slideshow, prevent buildup
             //allow override
             api.prevSlide();
           }

           // Right Arrow or Up Arrow
         } else if ((event.keyCode == 39) || (event.keyCode == 38)) {
           if (isGallery) {
             clearInterval(galVars.slideshow_interval); // Stop slideshow, prevent buildup
             if (event.keyCode == 39) {
              galTheme.nextSlide();
             } else {
               //up arrow => next gallery
               goToNextGalleryUrl();
             }
           } else {
             clearInterval(vars.slideshow_interval); // Stop slideshow, prevent buildup
             api.nextSlide();
           }

         // Spacebar
         } else if (event.keyCode == 32 && !galVars.hover_pause) {
           if (isGallery) {
             clearInterval(galVars.slideshow_interval); // Stop slideshow, prevent buildup
             galApi.playToggle();
           } else {
             clearInterval(vars.slideshow_interval); // Stop slideshow, prevent buildup
             api.playToggle();
           }
         }

       });

     }

     $(function () {
       $(window).trigger('resize');
     });
   }
);
