var messageDelay = 2000;  // How long to display status messages (in milliseconds)

$( init );

function init() {
  $('#newsletter').submit( submitForm );
  $('#sendingMessage').hide();
  $('#successMessage').hide();
  $('#failureMessage').hide();
  $('#incompleteMessage').hide();
  
}

function submitForm() {
  var newsletter = $(this);

  // Are all the fields filled in?

  if ( !$('#senderName').val() || !$('#senderEmail').val() ) {

    // No; display a warning message and return to the form
    $('#incompleteMessage').fadeIn().delay(messageDelay).fadeOut();

  } else {

    $('#sendingMessage').fadeIn();

    $.ajax( {
      url: newsletter.attr( 'action' ) + "?ajax=true",
      type: newsletter.attr( 'method' ),
      data: newsletter.serialize(),
      success: submitFinished
    } );
  }

  return false;
}

function submitFinished( response ) {
  response = $.trim( response );
  $('#sendingMessage').fadeOut();

  if ( response == "success" ) {

    $('#successMessage').fadeIn().delay(messageDelay).fadeOut();
    $('#senderName').val( "" );
    $('#senderEmail').val( "" );
    $('#message').val( "" );

    //$('#hiddenDiv').delay(messageDelay+500).fadeTo( 'slow', 1 );

  } else {

    $('#failureMessage').fadeIn().delay(messageDelay).fadeOut();

  }
}