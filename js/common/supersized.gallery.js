/*

 Supersized - Fullscreen Slideshow jQuery Plugin
 Version : 3.2.6
 Site	: www.buildinternet.com/project/supersized

 Author	: Sam Dunn
 Company : One Mighty Roar (www.onemightyroar.com)
 License : MIT License / GPL License

 */

(function ($) {


  $.gallery = function (options, callback) {

    if ($('#gallery-loader').length == 0 && $('#gallery').length == 0) {
      $('section.gallery').append('<div id="gallery-loader"></div><ul id="gallery"></ul>');
    }
    $('#projectInformation').css('left', -$(window).innerWidth() + 'px');

    /* Variables
     ----------------------------*/
    var el = '#gallery',
       base = this;
    // Access to jQuery and DOM versions of element
    base.$el = $(el);
    base.el = el;
    galVars = $.gallery.galVars;
    // Add a reverse reference to the DOM object
    base.$el.data('gallery', base);
    galApi = base.$el.data('gallery');
    base.init = function () {
      // Combine options and vars
      $.gallery.galVars = $.extend($.gallery.galVars, $.gallery.themeVars);
      $.gallery.galVars.options = $.extend({}, $.gallery.defaultOptions, $.gallery.themeOptions, options);
      base.options = $.gallery.galVars.options;

      base._build();
    };


    /* Build Elements
     ----------------------------*/
    base._build = function () {
      // Add in slide markers
      var thisSlide = 0,
         slideSet = '',
         markers = '',
         markerContent;
      //work around
      if (base.options.slides.length == base.options.start_slide - 1) {
        base.options.start_slide -=1;
      } /*else if (base.options.start_slide == 2) {
        base.options.start_slide = 1;
      }*/

      while (thisSlide <= base.options.slides.length - 1) {
        //Determine slide link content
        switch (base.options.slide_links) {
          case 'num':
            markerContent = thisSlide;
            break;
          case 'name':
            markerContent = base.options.slides[thisSlide].title;
            break;
          case 'blank':
            markerContent = '';
            break;
        }

        slideSet = slideSet + '<li class="slide-' + thisSlide + '"></li>';

        if (thisSlide == base.options.start_slide - 1) {
          // Slide links
          if (base.options.slide_links && galApi.getField('image', thisSlide) != null) markers = markers + '<li class="slide-link-' + thisSlide + ' current-slide"><a>' + markerContent + '</a></li>';

        } else {
          // Slide links
          if (base.options.slide_links && galApi.getField('image', thisSlide)) markers = markers + '<li class="slide-link-' + thisSlide + '" ><a>' + markerContent + '</a></li>';
        }
        thisSlide++;
      }

      if (base.options.slide_links) $(galVars.slide_list).html(markers);

      $(base.el).append(slideSet);


      base._start(); // Get things started
    };

    /**
     * Plays vimeo video
     */
    function playVimeo() {

      var currentActiveSlideEl = $('#gallery li:eq(' + galVars.current_slide + ')');
      var ifr = currentActiveSlideEl.find('iframe');

      if (ifr.length > 0) {
        $.log('play vimeo on ifr', ifr);

        $f(ifr[0]).addEvent('ready', function (playerId) {
          $.log('player ready to play', playerId);
          var froogaloop = $f(playerId);
          froogaloop.api('play');
        });
      }
    }

    function vimeoCurrentlyShown() {
      var currentSlideEl = $(base.el + ' li:eq(' + galVars.current_slide + ')');
      var isCurrentlyShown = (currentSlideEl.find('iframe').length > 0);
      return isCurrentlyShown;
    }

    /**
     * Resizes the video frame for vimeo
     *
     * @param currentSlideEl
     */
    function resizeVimeoIframe(currentFrame) {
      if (currentFrame) {
        var videoMaxWidth = galVars.options.vimeoMaxWidth;
        var videoMaxHeight = galVars.options.vimeoMaxHeight;

        var windowHeight = $(window).innerHeight();
        var windowWidth = $(window).innerWidth();
        var videoWidth = Math.min(windowWidth, videoMaxWidth);
        var videoHeight = videoWidth * videoMaxHeight / videoMaxWidth;
        //$.log('videoWidth', videoWidth);
        //$.log('videoHeight', videoHeight);
        var topPos = windowHeight > videoHeight ? Math.floor((windowHeight - videoHeight) / 2) : 0;
        var leftPos = windowWidth > videoWidth ? Math.floor((windowWidth - videoWidth) / 2) : 0;
        currentFrame.css({
          position:'absolute',
          width:videoWidth,
          height:videoHeight,
          top:topPos + 'px',
          left:leftPos + 'px'
        });
        //$.log('currentFrame', currentFrame);
      }
    }

    /**
     * Shows load item only for images
     */
    function showLoadItem() {
      var currentSlideEl = $(base.el + ' li:eq(' + galVars.current_slide + ')');
      if (currentSlideEl.find('img').length > 0) {
        $('.load-item').show();
      } else {
        $('.load-item').hide();
      }
    }


    function loadIframeVimeo(slideIndex) {

      //check if already loaded, do nothing
      if ($('#vimeo-' + slideIndex).length > 0) {
        $.log('loaded already');
        return;
      }

      var vimeo = galApi.getField('vimeo', slideIndex);
      var clipId = vimeo.substring(vimeo.lastIndexOf('/') + 1, vimeo.length);
      var playerId = 'vimeo-' + slideIndex;
      var autoPlay = 0;
      if (slideIndex === galVars.current_slide) {
        autoPlay = 1;
      }
      var vimeoEmbedStr = '<iframe id="' + playerId + '" src="http://player.vimeo.com/video/' + clipId + '?api=1&player_id=' + playerId + '&autoplay='+autoPlay+'" width="' + galVars.options.vimeoMaxWidth + '" height="' + galVars.options.vimeoMaxHeight + '" frameborder="0"></iframe>​';
      var vimeoEmbed = $(vimeoEmbedStr);
      resizeVimeoIframe(vimeoEmbed);
      var currentSlideEl = base.el + ' li:eq(' + slideIndex + ')';
      vimeoEmbed.appendTo(currentSlideEl).parent().addClass('activeslide');
    }

    /* Initialize
     ----------------------------*/
    base._start = function () {
      $('.load-item').hide();
      // Determine if starting slide random
      if (base.options.start_slide) {
        galVars.current_slide = base.options.start_slide - 1;
      } else {
        galVars.current_slide = Math.floor(Math.random() * base.options.slides.length);	// Generate random slide number
      }


      // If links should open in new window
      var linkTarget = base.options.new_window ? ' target="_blank"' : '';

      // Set slideshow quality (Supported only in FF and IE, no Webkit)
      if (base.options.performance == 3) {
        base.$el.addClass('speed'); 		// Faster transitions
      } else if ((base.options.performance == 1) || (base.options.performance == 2)) {
        base.$el.addClass('quality');	// Higher image quality
      }

      // Shuffle slide order if needed
      if (base.options.random) {
        arr = base.options.slides;
        for (var j, x, i = arr.length; i; j = parseInt(Math.random() * i), x = arr[--i], arr[i] = arr[j], arr[j] = x);	// Fisher-Yates shuffle algorithm (jsfromhell.com/array/shuffle)
        base.options.slides = arr;
      }


      /*-----Load initial set of images-----*/

      if (base.options.slides.length > 1) {
        if (base.options.slides.length > 2) {
          // Set previous image
          galVars.current_slide - 1 < 0 ? loadPrev = base.options.slides.length - 1 : loadPrev = galVars.current_slide - 1;	// If slide is 1, load last slide as previous
          if (galApi.getField('image', loadPrev) != null) {
            var imageLink = (base.options.slides[loadPrev].url) ? "href='" + base.options.slides[loadPrev].url + "'" : "";

            var imgPrev = $('<img src="' + galApi.getField('image', loadPrev) + '"/>');
            var slidePrev = base.el + ' li:eq(' + loadPrev + ')';
            imgPrev.appendTo(slidePrev).wrap('<a ' + imageLink + linkTarget + '></a>').parent().parent().addClass('image-loading prevslide');

            imgPrev.load(function () {
              $(this).data('origWidth', $(this).width()).data('origHeight', $(this).height());
              base.resizeNow();	// Resize background image
            });	// End Load
          }
        }
      } else {
        // Slideshow turned off if there is only one slide
        base.options.slideshow = 0;
      }


      // Set current image
      if (galApi.getField('image') != null) {
        imageLink = (galApi.getField('url')) ? "href='" + galApi.getField('url') + "'" : "";
        var img = $('<img src="' + galApi.getField('image') + '"/>');

        var slideCurrent = base.el + ' li:eq(' + galVars.current_slide + ')';
        img.appendTo(slideCurrent).wrap('<a ' + imageLink + linkTarget + '></a>').parent().parent().addClass('image-loading activeslide');

        img.load(function () {
          base._origDim($(this));
          base.resizeNow();	// Resize background image
          base.launch();
          if (typeof galTheme != 'undefined' && typeof galTheme._init == "function") galTheme._init();	// Load galTheme
        });
      } else if (galApi.getField('vimeo') != null) {
        loadIframeVimeo(galVars.current_slide);
        base.launch();
        if (typeof galTheme != 'undefined' && typeof galTheme._init == "function") galTheme._init();	// Load galTheme
      }

      if (base.options.slides.length > 1) {
        // Set next image
        galVars.current_slide == base.options.slides.length - 1 ? loadNext = 0 : loadNext = galVars.current_slide + 1;	// If slide is last, load first slide as next
        var slideNext = base.el + ' li:eq(' + loadNext + ')';
        if (galApi.getField('image', loadNext) != null) {
          imageLink = (base.options.slides[loadNext].url) ? "href='" + base.options.slides[loadNext].url + "'" : "";

          var imgNext = $('<img src="' + base.options.slides[loadNext].image + '"/>');
          imgNext.appendTo(slideNext).wrap('<a ' + imageLink + linkTarget + '></a>').parent().parent().addClass('image-loading');

          imgNext.load(function () {
            $(this).data('origWidth', $(this).width()).data('origHeight', $(this).height());
            base.resizeNow();	// Resize background image
          });	// End Load
        } else if (galApi.getField('vimeo', loadNext) != null) {
          loadIframeVimeo(loadNext);
          $(slideNext).removeClass('activeslide').addClass('nextslide');
        }
      }
      /*-----End load initial images-----*/

    };


    /* Launch gallery
     ----------------------------*/
    base.launch = function () {
      base.$el.css('visibility', 'visible');
      $('#gallery-loader').remove();		//Hide loading animation

      // Call galTheme function for before slide transition
      if (typeof galTheme != 'undefined' && typeof galTheme.beforeAnimation == "function") galTheme.beforeAnimation('next');


      // Pause when hover on image
      if (base.options.slideshow && base.options.pause_hover) {
        $(base.el).hover(function () {
          if (galVars.in_animation) return false;		// Abort if currently animating
          galVars.hover_pause = true;	// Mark slideshow paused from hover
          if (!galVars.is_paused) {
            galVars.hover_pause = 'resume';	// It needs to resume afterwards
            base.playToggle();
          }
        }, function () {
          if (galVars.hover_pause == 'resume') {
            base.playToggle();
            galVars.hover_pause = false;
          }
        });
      }

      if (base.options.slide_links) {
        // Slide marker clicked
        $(galVars.slide_list + '> li').click(function () {

          index = $(galVars.slide_list + '> li').index(this);
          targetSlide = index + 1;

          base.goTo(targetSlide);
          return false;

        });
      }

      // Start slideshow if enabled
      if (base.options.slideshow && base.options.slides.length > 1) {

        // Start slideshow if autoplay enabled
        if (base.options.autoplay && base.options.slides.length > 1) {
          galVars.slideshow_interval = setInterval(base.nextSlide, base.options.slide_interval);	// Initiate slide interval
        } else {
          galVars.is_paused = true;	// Mark as paused
        }

        //Prevent navigation items from being dragged

        $('.load-item img').bind("contextmenu mousedown", function () {
          return false;
        });

      }

      // Adjust image when browser is resized
      $(window).resize(function () {
        if (vimeoCurrentlyShown()) {
          var currentIFrame = $(base.el + ' li:eq(' + galVars.current_slide + ')').find('iframe')[0];
          resizeVimeoIframe($(currentIFrame));
        }
        base.resizeNow();
      });
      callback();
    };

    /* Resize Images
     ----------------------------*/
    base.resizeNow = function () {

      return base.$el.each(function () {
        //  Resize each image seperately
        $('img', base.el).each(function () {

          thisSlide = $(this);
          var ratio = (thisSlide.data('origHeight') / thisSlide.data('origWidth')).toFixed(2);	// Define image ratio

          // Gather browser size
          var browserwidth = base.$el.width(),
             browserheight = base.$el.height(),
             offset;

          /*-----Resize Image-----*/
          if (base.options.fit_always) {  // Fit always is enabled
            if ((browserheight / browserwidth) > ratio) {
              resizeWidth();
            } else {
              resizeHeight();
            }
          } else {  // Normal Resize
            if ((browserheight <= base.options.min_height) && (browserwidth <= base.options.min_width)) {  // If window smaller than minimum width and height

              if ((browserheight / browserwidth) > ratio) {
                base.options.fit_landscape && ratio < 1 ? resizeWidth(true) : resizeHeight(true);	// If landscapes are set to fit
              } else {
                base.options.fit_portrait && ratio >= 1 ? resizeHeight(true) : resizeWidth(true);		// If portraits are set to fit
              }

            } else if (browserwidth <= base.options.min_width) {    // If window only smaller than minimum width

              if ((browserheight / browserwidth) > ratio) {
                base.options.fit_landscape && ratio < 1 ? resizeWidth(true) : resizeHeight();	// If landscapes are set to fit
              } else {
                base.options.fit_portrait && ratio >= 1 ? resizeHeight() : resizeWidth(true);		// If portraits are set to fit
              }

            } else if (browserheight <= base.options.min_height) {  // If window only smaller than minimum height

              if ((browserheight / browserwidth) > ratio) {
                base.options.fit_landscape && ratio < 1 ? resizeWidth() : resizeHeight(true);	// If landscapes are set to fit
              } else {
                base.options.fit_portrait && ratio >= 1 ? resizeHeight(true) : resizeWidth();		// If portraits are set to fit
              }

            } else {  // If larger than minimums

              if ((browserheight / browserwidth) > ratio) {
                base.options.fit_landscape && ratio < 1 ? resizeWidth() : resizeHeight();	// If landscapes are set to fit
              } else {
                base.options.fit_portrait && ratio >= 1 ? resizeHeight() : resizeWidth();		// If portraits are set to fit
              }

            }

          }
          /*-----End Image Resize-----*/

          /*-----Resize Functions-----*/

          function resizeWidth(minimum) {
            if (minimum) {  // If minimum height needs to be considered
              if (thisSlide.width() < browserwidth || thisSlide.width() < base.options.min_width) {
                if (thisSlide.width() * ratio >= base.options.min_height) {
                  thisSlide.width(base.options.min_width);
                  thisSlide.height(thisSlide.width() * ratio);
                } else {
                  resizeHeight();
                }
              }
            } else {
              if (base.options.min_height >= browserheight && !base.options.fit_landscape) {  // If minimum height needs to be considered
                if (browserwidth * ratio >= base.options.min_height || (browserwidth * ratio >= base.options.min_height && ratio <= 1)) {  // If resizing would push below minimum height or image is a landscape
                  thisSlide.width(browserwidth);
                  thisSlide.height(browserwidth * ratio);
                } else if (ratio > 1) {    // Else the image is portrait
                  thisSlide.height(base.options.min_height);
                  thisSlide.width(thisSlide.height() / ratio);
                } else if (thisSlide.width() < browserwidth) {
                  thisSlide.width(browserwidth);
                  thisSlide.height(thisSlide.width() * ratio);
                }
              } else {  // Otherwise, resize as normal
                thisSlide.width(browserwidth);
                thisSlide.height(browserwidth * ratio);
              }
            }
          }

          ;

          function resizeHeight(minimum) {
            if (minimum) {  // If minimum height needs to be considered
              if (thisSlide.height() < browserheight) {
                if (thisSlide.height() / ratio >= base.options.min_width) {
                  thisSlide.height(base.options.min_height);
                  thisSlide.width(thisSlide.height() / ratio);
                } else {
                  resizeWidth(true);
                }
              }
            } else {  // Otherwise, resized as normal
              if (base.options.min_width >= browserwidth) {  // If minimum width needs to be considered
                if (browserheight / ratio >= base.options.min_width || ratio > 1) {  // If resizing would push below minimum width or image is a portrait
                  thisSlide.height(browserheight);
                  thisSlide.width(browserheight / ratio);
                } else if (ratio <= 1) {    // Else the image is landscape
                  thisSlide.width(base.options.min_width);
                  thisSlide.height(thisSlide.width() * ratio);
                }
              } else {  // Otherwise, resize as normal
                thisSlide.height(browserheight);
                thisSlide.width(browserheight / ratio);
              }
            }
          }

          ;

          /*-----End Resize Functions-----*/

          if (thisSlide.parents('li').hasClass('image-loading')) {
            $('.image-loading').removeClass('image-loading');
          }

          // Horizontally Center
          if (base.options.horizontal_center) {
            $(this).css('left', (browserwidth - $(this).width()) / 2);
          }

          // Vertically Center
          if (base.options.vertical_center) {
            $(this).css('top', (browserheight - $(this).height()) / 2);
          }

        });

        // Basic image drag and right click protection
        if (base.options.image_protect) {

          $('img', base.el).bind("contextmenu mousedown", function () {
            return false;
          });

        }

        return false;

      });

    };


    /**
     * Resize the item dimensions if it's bigger than the viewport
     * @param width {integer} Width of the item to be opened
     * @param height {integer} Height of the item to be opened
     * @return An array containin the "fitted" dimensions
     */
    function _fitToViewport(width, height) {
      resized = false;

      _getDimensions(width, height);

      // Define them in case there's no resize needed
      imageWidth = width, imageHeight = height;

      if (((pp_containerWidth > windowWidth) || (pp_containerHeight > windowHeight)) && doresize && settings.allow_resize && !percentBased) {
        resized = true, fitting = false;

        while (!fitting) {
          if ((pp_containerWidth > windowWidth)) {
            imageWidth = (windowWidth - 200);
            imageHeight = (height / width) * imageWidth;
          } else if ((pp_containerHeight > windowHeight)) {
            imageHeight = (windowHeight - 200);
            imageWidth = (width / height) * imageHeight;
          } else {
            fitting = true;
          }
          ;

          pp_containerHeight = imageHeight, pp_containerWidth = imageWidth;
        }
        ;

        _getDimensions(imageWidth, imageHeight);

        if ((pp_containerWidth > windowWidth) || (pp_containerHeight > windowHeight)) {
          _fitToViewport(pp_containerWidth, pp_containerHeight)
        }
        ;
      }
      ;

      return {
        width:Math.floor(imageWidth),
        height:Math.floor(imageHeight),
        containerHeight:Math.floor(pp_containerHeight),
        containerWidth:Math.floor(pp_containerWidth) + (settings.horizontal_padding * 2),
        contentHeight:Math.floor(pp_contentHeight),
        contentWidth:Math.floor(pp_contentWidth),
        resized:resized
      };
    }

    ;

    /**
     * Get the containers dimensions according to the item size
     * @param width {integer} Width of the item to be opened
     * @param height {integer} Height of the item to be opened
     */
    function _getDimensions(width, height) {
      width = parseFloat(width);
      height = parseFloat(height);

      // Get the details height, to do so, I need to clone it since it's invisible
      $pp_details = $pp_pic_holder.find('.pp_details');
      $pp_details.width(width);
      detailsHeight = parseFloat($pp_details.css('marginTop')) + parseFloat($pp_details.css('marginBottom'));

      $pp_details = $pp_details.clone().addClass(settings.galTheme).width(width).appendTo($('body')).css({
        'position':'absolute',
        'top':-10000
      });
      detailsHeight += $pp_details.height();
      detailsHeight = (detailsHeight <= 34) ? 36 : detailsHeight; // Min-height for the details
      if ($.browser.msie && $.browser.version == 7) detailsHeight += 8;
      $pp_details.remove();

      // Get the titles height, to do so, I need to clone it since it's invisible
      $pp_title = $pp_pic_holder.find('.ppt');
      $pp_title.width(width);
      titleHeight = parseFloat($pp_title.css('marginTop')) + parseFloat($pp_title.css('marginBottom'));
      $pp_title = $pp_title.clone().appendTo($('body')).css({
        'position':'absolute',
        'top':-10000
      });
      titleHeight += $pp_title.height();
      $pp_title.remove();

      // Get the container size, to resize the holder to the right dimensions
      pp_contentHeight = height + detailsHeight;
      pp_contentWidth = width;
      pp_containerHeight = pp_contentHeight + titleHeight + $pp_pic_holder.find('.pp_top').height() + $pp_pic_holder.find('.pp_bottom').height();
      pp_containerWidth = width;
    }

    base.getFileType = function (itemSrc) {
      if (itemSrc.match(/youtube\.com\/watch/i) || itemSrc.match(/youtu\.be/i)) {
        return 'youtube';
      } else if (itemSrc.match(/vimeo\.com/i)) {
        return 'vimeo';
      } else if (itemSrc.match(/\b.mov\b/i)) {
        return 'quicktime';
      } else if (itemSrc.match(/\b.swf\b/i)) {
        return 'flash';
      } else if (itemSrc.match(/\biframe=true\b/i)) {
        return 'iframe';
      } else if (itemSrc.match(/\bajax=true\b/i)) {
        return 'ajax';
      } else if (itemSrc.match(/\bcustom=true\b/i)) {
        return 'custom';
      } else if (itemSrc.substr(0, 1) == '#') {
        return 'inline';
      } else {
        return 'image';
      }
      ;
    };

    //for repload images only
    base.loadSlide = function(slideNumber, $targetList) {
      if (slideNumber > base.options.slides.length) {
        slideNumber = 0;
      } else if (slideNumber == - 1) {
        slideNumber = base.options.slides.length - 1;
      }

      //$.log('slideNumber', slideNumber);
      //$.log('$targetList.html()', $targetList.html());
      if (!$targetList.html()) {
        if (galApi.getField('image', slideNumber) != null) {
          // If links should open in new window
          var linkTarget = base.options.new_window ? ' target="_blank"' : '';

          var imageLink = (base.options.slides[slideNumber].url) ? "href='" + base.options.slides[slideNumber].url + "'" : "";	// If link exists, build it
          var img = $('<img src="' + base.options.slides[slideNumber].image + '"/>');

          img.appendTo($targetList).wrap('<a ' + imageLink + linkTarget + '></a>').parent().parent().addClass('image-loading').css('visibility', 'hidden');

          img.load(function () {
            base._origDim($(this));
            base.resizeNow();
          });	// End Load
        }
      }
    }


    /* Next Slide
     ----------------------------*/
    base.nextSlide = function () {
      $.log('base.nextSlide()');
      if (galVars.in_animation || !galApi.options.slideshow) return false;    // Abort if currently animating
      else galVars.in_animation = true;		// Otherwise set animation marker

      clearInterval(galVars.slideshow_interval);	// Stop slideshow

      var slides = base.options.slides, // Pull in slides array
         liveslide = base.$el.find('.activeslide');		// Find active slide
      $('.prevslide').removeClass('prevslide');
      liveslide.removeClass('activeslide').addClass('prevslide');	// Remove active class & update previous slide

      galVars.current_slide = galVars.current_slide == slides.length - 1 ? 0 : galVars.current_slide += 1;	// Determine next slide

      var nextslide = $(base.el + ' li:eq(' + galVars.current_slide + ')');
      var prevslide = base.$el.find('.prevslide');

      // If hybrid mode is on drop quality for transition
      if (base.options.performance == 1) base.$el.removeClass('quality').addClass('speed');

      /*-----Load Image-----*/

      var loadSlide;
      loadSlide = galVars.current_slide;
      $.log("loadSlide: " + loadSlide);


      //Check to see if we need to show the info

      var targetList = base.el + ' li:eq(' + loadSlide + ')';
      if (!$(targetList).html()) {
        if (galApi.getField('image', loadSlide) != null) {
          // If links should open in new window
          var linkTarget = base.options.new_window ? ' target="_blank"' : '';

          imageLink = (base.options.slides[loadSlide].url) ? "href='" + base.options.slides[loadSlide].url + "'" : "";	// If link exists, build it
          var img = $('<img src="' + base.options.slides[loadSlide].image + '"/>');

          img.appendTo(targetList).wrap('<a ' + imageLink + linkTarget + '></a>').parent().parent().addClass('image-loading').css('visibility', 'hidden');

          img.load(function () {
            base._origDim($(this));
            base.resizeNow();
          });	// End Load
        } else if (galApi.getField('vimeo', loadSlide) != null) {
          $.log('loadIframeVimeo(loadSlice)', loadSlide);
          loadIframeVimeo(loadSlide);
        }
      };
      var nextLoadSlide = loadSlide + 1;
      var nextTargetList = base.el + ' li:eq(' + nextLoadSlide + ')';
      //$.log('nextTargetList', $(nextTargetList));
      if ($(nextTargetList).length > 0) {
        base.loadSlide(nextLoadSlide, $(nextTargetList));
      }

      /*-----End Load Image-----*/

      // Call theme function for before slide transition
      if (typeof galTheme != 'undefined' && typeof galTheme.beforeAnimation == "function") galTheme.beforeAnimation('next');

      //Update slide markers
      if (base.options.slide_links) {
        $('.current-slide').removeClass('current-slide');
        nextslide.addClass('current-slide');
      }

      nextslide.css('visibility', 'hidden').addClass('activeslide');	// Update active slide

      switch (base.options.transition) {
        case 0:
        case 'none':  // No transition
          nextslide.css('visibility', 'visible');
          galVars.in_animation = false;
          break;
        case 1:
        case 'fade':  // Fade
          nextslide.animate({opacity:0}, 0).css('visibility', 'visible').animate({opacity:1, avoidTransforms:false}, base.options.transition_speed, function () {
            base.afterAnimation();
          });
          break;
        case 2:
        case 'slideTop':  // Slide Top
          nextslide.animate({top:-base.$el.height()}, 0).css('visibility', 'visible').animate({ top:0, avoidTransforms:false }, base.options.transition_speed, function () {
            base.afterAnimation();
          });
          break;
        case 3:
        case 'slideRight':  // Slide Right
          nextslide.animate({left:base.$el.width()}, 0).css('visibility', 'visible').animate({ left:0, avoidTransforms:false }, base.options.transition_speed, function () {
            base.afterAnimation();
          });
          break;
        case 4:
        case 'slideBottom': // Slide Bottom
          nextslide.animate({top:base.$el.height()}, 0).css('visibility', 'visible').animate({ top:0, avoidTransforms:false }, base.options.transition_speed, function () {
            base.afterAnimation();
          });
          break;
        case 5:
        case 'slideLeft':  // Slide Left
          nextslide.animate({left:-base.$el.width()}, 0).css('visibility', 'visible').animate({ left:0, avoidTransforms:false }, base.options.transition_speed, function () {
            base.afterAnimation();
          });
          break;
        case 6:
        case 'carouselRight':  // Carousel Right

          nextslide.animate({left:base.$el.width()}, 0).css('visibility', 'visible').animate({ left:0, avoidTransforms:false }, base.options.transition_speed, function () {
            base.afterAnimation();
          });
          liveslide.animate({ left:-base.$el.width(), avoidTransforms:false }, base.options.transition_speed);

          break;
        case 7:
        case 'carouselLeft':   // Carousel Left
          nextslide.animate({left:-base.$el.width()}, 0).css('visibility', 'visible').animate({ left:0, avoidTransforms:false }, base.options.transition_speed, function () {
            base.afterAnimation();
          });
          liveslide.animate({ left:base.$el.width(), avoidTransforms:false }, base.options.transition_speed);
          break;
      }
      return false;
    };


    /* Previous Slide
     ----------------------------*/
    base.prevSlide = function () {
      $.log('base.prevSlide()');
      if (galVars.in_animation || !galApi.options.slideshow) return false;    // Abort if currently animating
      else galVars.in_animation = true;		// Otherwise set animation marker

      clearInterval(galVars.slideshow_interval);	// Stop slideshow

      var slides = base.options.slides, // Pull in slides array
         liveslide = base.$el.find('.activeslide');		// Find active slide
      $('.prevslide').removeClass('prevslide');
      liveslide.removeClass('activeslide').addClass('prevslide');		// Remove active class & update previous slide

      // Get current slide number
      galVars.current_slide = galVars.current_slide == 0 ? base.options.slides.length - 1 : galVars.current_slide -= 1;


      var nextslide = $(base.el + ' li:eq(' + galVars.current_slide + ')'),
         prevslide = base.$el.find('.prevslide');


      // If hybrid mode is on drop quality for transition
      if (base.options.performance == 1) base.$el.removeClass('quality').addClass('speed');


      /*-----Load Image-----*/

      var loadSlide = galVars.current_slide;
      $.log('loadSlide', loadSlide);

      var targetList = base.el + ' li:eq(' + loadSlide + ')';
      if (!$(targetList).html()) {
        // If links should open in new window
        if (galApi.getField('image', loadSlide) != null) {
          var linkTarget = base.options.new_window ? ' target="_blank"' : '';
          imageLink = (base.options.slides[loadSlide].url) ? "href='" + base.options.slides[loadSlide].url + "'" : "";	// If link exists, build it
          var img = $('<img src="' + base.options.slides[loadSlide].image + '"/>');

          img.appendTo(targetList).wrap('<a ' + imageLink + linkTarget + '></a>').parent().parent().addClass('image-loading').css('visibility', 'hidden');

          img.load(function () {
            base._origDim($(this));
            base.resizeNow();
          });	// End Load
        } else if (galApi.getField('vimeo', loadSlide) != null) {
          loadIframeVimeo(loadSlide);
        }
      }
      ;

      var prevLoadSlide = loadSlide - 1;
      var prevTargetList = base.el + ' li:eq(' + prevLoadSlide + ')';
      //$.log('prevTargetList', $(prevTargetList));
      if ($(prevTargetList).length > 0) {
        base.loadSlide(prevLoadSlide, $(prevTargetList));
      }

      // Call theme function for before slide transition
      if (typeof galTheme != 'undefined' && typeof galTheme.beforeAnimation == "function") galTheme.beforeAnimation('prev');

      //Update slide markers
      if (base.options.slide_links) {
        $('.current-slide').removeClass('current-slide');
        $(galVars.slide_list + '> li').eq(galVars.current_slide).addClass('current-slide');
      }

      nextslide.css('visibility', 'hidden').addClass('activeslide');	// Update active slide

      switch (base.options.transition) {
        case 0:
        case 'none':  // No transition
          nextslide.css('visibility', 'visible');
          galVars.in_animation = false;
          base.afterAnimation();
          break;
        case 1:
        case 'fade':  // Fade
          nextslide.animate({opacity:0}, 0).css('visibility', 'visible').animate({opacity:1, avoidTransforms:false}, base.options.transition_speed, function () {
            base.afterAnimation();
          });
          break;
        case 2:
        case 'slideTop':  // Slide Top (reverse)
          nextslide.animate({top:base.$el.height()}, 0).css('visibility', 'visible').animate({ top:0, avoidTransforms:false }, base.options.transition_speed, function () {
            base.afterAnimation();
          });
          break;
        case 3:
        case 'slideRight':  // Slide Right (reverse)
          nextslide.animate({left:-base.$el.width()}, 0).css('visibility', 'visible').animate({ left:0, avoidTransforms:false }, base.options.transition_speed, function () {
            base.afterAnimation();
          });
          break;
        case 4:
        case 'slideBottom': // Slide Bottom (reverse)
          nextslide.animate({top:-base.$el.height()}, 0).css('visibility', 'visible').animate({ top:0, avoidTransforms:false }, base.options.transition_speed, function () {
            base.afterAnimation();
          });
          break;
        case 5:
        case 'slideLeft':  // Slide Left (reverse)
          nextslide.animate({left:base.$el.width()}, 0).css('visibility', 'visible').animate({ left:0, avoidTransforms:false }, base.options.transition_speed, function () {
            base.afterAnimation();
          });
          break;
        case 6:
        case 'carouselRight':  // Carousel Right (reverse)
          nextslide.animate({left:-base.$el.width()}, 0).css('visibility', 'visible').animate({ left:0, avoidTransforms:false }, base.options.transition_speed, function () {
            base.afterAnimation();
          });
          liveslide.animate({left:0}, 0).animate({ left:base.$el.width(), avoidTransforms:false}, base.options.transition_speed);
          break;
        case 7:
        case 'carouselLeft':   // Carousel Left (reverse)
          nextslide.animate({left:base.$el.width()}, 0).css('visibility', 'visible').animate({ left:0, avoidTransforms:false }, base.options.transition_speed, function () {
            base.afterAnimation();
          });
          liveslide.animate({left:0}, 0).animate({ left:-base.$el.width(), avoidTransforms:false }, base.options.transition_speed);
          break;
      }
      return false;
    };


    /* Play/Pause Toggle
     ----------------------------*/
    base.playToggle = function () {

      if (galVars.in_animation || !galApi.options.slideshow) return false;		// Abort if currently animating

      if (galVars.is_paused) {

        galVars.is_paused = false;

        // Call theme function for play
        if (typeof galTheme != 'undefined' && typeof galTheme.playToggle == "function") galTheme.playToggle('play');

        // Resume slideshow
        galVars.slideshow_interval = setInterval(base.nextSlide, base.options.slide_interval);

      } else {

        galVars.is_paused = true;

        // Call theme function for pause
        if (typeof galTheme != 'undefined' && typeof galTheme.playToggle == "function") galTheme.playToggle('pause');

        // Stop slideshow
        clearInterval(galVars.slideshow_interval);

      }

      return false;

    };


    /* Go to specific slide
     ----------------------------*/
    base.goTo = function (targetSlide) {
      if (galVars.in_animation || !galApi.options.slideshow) return false;		// Abort if currently animating

      var totalSlides = base.options.slides.length;

      // If target outside range
      if (targetSlide < 0) {
        targetSlide = totalSlides;
      } else if (targetSlide > totalSlides) {
        targetSlide = 1;
      }
      targetSlide = totalSlides - targetSlide + 1;

      clearInterval(galVars.slideshow_interval);	// Stop slideshow, prevent buildup

      // Call theme function for goTo trigger
      if (typeof galTheme != 'undefined' && typeof galTheme.goTo == "function") galTheme.goTo();

      if (galVars.current_slide == totalSlides - targetSlide) {
        if (!(galVars.is_paused)) {
          galVars.slideshow_interval = setInterval(base.nextSlide, base.options.slide_interval);
        }
        return false;
      }

      // If ahead of current position
      if (totalSlides - targetSlide > galVars.current_slide) {

        // Adjust for new next slide
        galVars.current_slide = totalSlides - targetSlide - 1;
        galVars.update_images = 'next';
        base._placeSlide(galVars.update_images);

        //Otherwise it's before current position
      } else if (totalSlides - targetSlide < galVars.current_slide) {

        // Adjust for new prev slide
        galVars.current_slide = totalSlides - targetSlide + 1;
        galVars.update_images = 'prev';
        base._placeSlide(galVars.update_images);

      }

      // set active markers
      if (base.options.slide_links) {
        $(galVars.slide_list + '> .current-slide').removeClass('current-slide');
        $(galVars.slide_list + '> li').eq((totalSlides - targetSlide)).addClass('current-slide');
      }
    };


    /* Place Slide
     ----------------------------*/
    base._placeSlide = function (place) {
      $.log('base._placeSlide: place', place);

      // If links should open in new window
      var linkTarget = base.options.new_window ? ' target="_blank"' : '';

      loadSlide = false;

      if (place == 'next') {

        galVars.current_slide == base.options.slides.length - 1 ? loadSlide = 0 : loadSlide = galVars.current_slide + 1;	// Determine next slide

        $.log('galVars.current_slide of place == next', galVars.current_slide);
        $.log('loadSlide', loadSlide);

        var targetList = base.el + ' li:eq(' + loadSlide + ')';

        if (!$(targetList).html()) {
          if (base.options.slides[loadSlide]['image'] != null) {
            // If links should open in new window
            var linkTarget = base.options.new_window ? ' target="_blank"' : '';

            imageLink = (base.options.slides[loadSlide].url) ? "href='" + base.options.slides[loadSlide].url + "'" : "";	// If link exists, build it
            var img = $('<img src="' + base.options.slides[loadSlide].image + '"/>');

            img.appendTo(targetList).wrap('<a ' + imageLink + linkTarget + '></a>').parent().parent().addClass('image-loading').css('visibility', 'hidden');

            img.load(function () {
              base._origDim($(this));
              base.resizeNow();
            });	// End Load
          } else if (base.options.slides[loadSlide]['vimeo'] != null) {
            $.log('Vimeo Loading.... of place = next');
          }

        }
        base.nextSlide();

      } else if (place == 'prev') {

        galVars.current_slide - 1 < 0 ? loadSlide = base.options.slides.length - 1 : loadSlide = galVars.current_slide - 1;	// Determine next slide

        var targetList = base.el + ' li:eq(' + loadSlide + ')';

        if (!$(targetList).html()) {
          if (base.options.slides[loadSlide]['image'] != null) {
            // If links should open in new window
            var linkTarget = base.options.new_window ? ' target="_blank"' : '';

            imageLink = (base.options.slides[loadSlide].url) ? "href='" + base.options.slides[loadSlide].url + "'" : "";	// If link exists, build it
            var img = $('<img src="' + base.options.slides[loadSlide].image + '"/>');

            img.appendTo(targetList).wrap('<a ' + imageLink + linkTarget + '></a>').parent().parent().addClass('image-loading').css('visibility', 'hidden');

            img.load(function () {
              base._origDim($(this));
              base.resizeNow();
            });	// End Load
          } else if (base.options.slides[loadSlide]['vimeo'] != null) {
            $.log('Vimeo Loading.... of place = prev');
          }
        }

        base.prevSlide();
      }

    };


    /* Get Original Dimensions
     ----------------------------*/
    base._origDim = function (targetSlide) {
      targetSlide.data('origWidth', targetSlide.width()).data('origHeight', targetSlide.height());
    };


    /* After Slide Animation
     ----------------------------*/

    base.afterAnimation = function () {
      $.log('afterAnimation callback from gallery');
      if (vimeoCurrentlyShown()) {
        var currentIFrame = $(base.el + ' li:eq(' + galVars.current_slide + ')').find('iframe')[0];
        resizeVimeoIframe($(currentIFrame));
      } else {
        base.resizeNow();
      }
      showLoadItem();
      playVimeo();
      // If hybrid mode is on swap back to higher image quality
      if (base.options.performance == 1) {
        base.$el.removeClass('speed').addClass('quality');
      }

      // Update previous slide
      if (galVars.update_images) {
        galVars.current_slide - 1 < 0 ? setPrev = base.options.slides.length - 1 : setPrev = galVars.current_slide - 1;
        galVars.update_images = false;
        $('.prevslide').removeClass('prevslide');
        $(base.el + ' li:eq(' + setPrev + ')').addClass('prevslide');
      }

      galVars.in_animation = false;

      // Resume slideshow
      if (!galVars.is_paused && base.options.slideshow) {
        galVars.slideshow_interval = setInterval(base.nextSlide, base.options.slide_interval);
        if (base.options.stop_loop && galVars.current_slide == base.options.slides.length - 1) base.playToggle();
      }

      // Call theme function for after slide transition
      if (typeof galTheme != 'undefined' && typeof galTheme.afterAnimation == "function") galTheme.afterAnimation();

      return false;

    };

    /**
     * Gets the field defined on json data.
     *
     * @param field the field name
     * @param slideIndex optional the slideIndex, if not specified, slideIndex = current_slide.
     * @return The field value
     */
    base.getField = function (field, slideIndex) {
      if (slideIndex == undefined) {
        slideIndex = galVars.current_slide;
      }
      return base.options.slides[slideIndex][field];
    };

    // Make it go!
    base.init();
  };


  /* Global Variables
   ----------------------------*/
  $.gallery.galVars = {

    // Elements
    slide_list:'#slide-list', // Slide link list

    // Internal variables
    img_counter:0,
    current_slide:0, // Current slide number
    in_animation:false, // Prevents animations from stacking
    is_paused:false, // Tracks paused on/off
    hover_pause:false, // If slideshow is paused from hover
    slideshow_interval:false, // Stores slideshow timer
    update_images:false, // Trigger to update images after slide jump
    options:{}      // Stores assembled options list

  };


  /* Default Options
   ----------------------------*/
  $.gallery.defaultOptions = {

    slideshow:1, // Slideshow on/off
    autoplay:1, // Slideshow starts playing automatically
    start_slide:1, // Start slide (0 is random)
    stop_loop:0, // Stops slideshow on last slide
    random:0, // Randomize slide order (Ignores start slide)
    slide_interval:5000, // Length between transitions
    transition:1, // 0-None, 1-Fade, 2-Slide Top, 3-Slide Right, 4-Slide Bottom, 5-Slide Left, 6-Carousel Right, 7-Carousel Left
    transition_speed:750, // Speed of transition
    new_window:1, // Image links open in new window/tab
    pause_hover:0, // Pause slideshow on hover
    keyboard_nav:1, // Keyboard navigation on/off
    performance:1, // 0-Normal, 1-Hybrid speed/quality, 2-Optimizes image quality, 3-Optimizes transition speed //  (Only works for Firefox/IE, not Webkit)
    image_protect:1, // Disables image dragging and right click with Javascript

    fit_always:0, // Image will never exceed browser width or height (Ignores min. dimensions)
    fit_landscape:0, // Landscape images will not exceed browser width
    fit_portrait:1, // Portrait images will not exceed browser height
    min_width:0, // Min width allowed (in pixels)
    min_height:0, // Min height allowed (in pixels)
    horizontal_center:1, // Horizontally center background
    vertical_center:1, // Vertically center background

    slide_links:1, // Individual links for each slide (Options: false, 'num', 'name', 'blank')
    vimeoMaxWidth:800,
    vimeoMaxHeight:400
  };

  $.fn.gallery = function (options) {
    return this.each(function () {
      (new $.gallery(options));
    });
  };

})(jQuery);

