/*

 Supersized - Fullscreen Slideshow jQuery Plugin
 Version : 3.2.6
 Theme 	: Shutter 1.1

 Site	: www.buildinternet.com/project/supersized
 Author	: Sam Dunn
 Company : One Mighty Roar (www.onemightyroar.com)
 License : MIT License / GPL License

 */

(function ($) {

  var currentSlideCounter = 1;

  var infoShown = false;

  function showInfo(finishCallback) {
    infoShown = true;

    $('#projectInformation').css({'opacity':'0', 'left':'30'}).show();
    $('#projectInformation').animate({'opacity':'1'}, 500, 'easeInOutExpo');

    $('#gallery').animate({'opacity':'0'}, 500);
    $('.caption').animate({'opacity':'0'}, 500);

    $('#projectInformation').css('left', $(window).innerWidth() + 'px');
    //alert($('#projectInformation').css('left'));
    $('#projectInformation').animate(
       { left:30 },
       1000,
       'easeInOutExpo',
       function() {
         $.gallery.themeVars.projectInfo.resize();
         finishCallback();
       }
    );

    // Update slide number
    updateSlideNumber();

  }

  function updateSlideNumber() {
    $.log('currentSlideCounter', currentSlideCounter);
    if (galVars.slide_current.length) {
      $(galVars.slide_current).html(currentSlideCounter);
    }
    updateHashValue();
  }

  function hideInfo(finishCallback) {
    infoShown = false;

    //$('#projectInformation').css('left', (-$(window).innerHeight()) + 'px');

    $('#projectInformation').animate({'opacity':'0'}, 500, 'easeInOutExpo', function () {
      $('#projectInformation').hide()
    });

    $('#gallery').delay(500).animate({'opacity':'1'}, 500);
    $('.caption').delay(500).animate({'opacity':'1'}, 500);

    var goalX = (-$(window).innerHeight()) + 'px';
    $('#projectInformation').animate(
       { left:goalX },
       1000,
       'easeInOutExpo',
       finishCallback
    ).hide();
    updateSlideNumber();
  }

  function handleNext() {
    pauseVimeo();
    if (currentSlideCounter == 1) {
      currentSlideCounter += 1;
      showInfo(function () {
        //callback
      });
    } else if (currentSlideCounter == 2) {
      currentSlideCounter += 1;
      hideInfo(function () {
        //callback
      });
      galApi.goTo(2);
    } else {
      if (currentSlideCounter > galVars.options.slides.length) {
        currentSlideCounter = 1;
      } else {
        currentSlideCounter += 1;
      }
      updateSlideNumber();
      galApi.nextSlide();
    }

  }

  function handlePrevious() {
    pauseVimeo();
    if (currentSlideCounter == 2) {
      currentSlideCounter -= 1;
      hideInfo(function () {
        //callback
      });
      galApi.goTo(1);
    } else if (currentSlideCounter == 3) {
      currentSlideCounter -= 1;
      showInfo(function () {
        //callback
      });
    } else {
      if (currentSlideCounter == 1) {
        currentSlideCounter = galVars.options.slides.length + 1;
      } else {
        currentSlideCounter -= 1;
      }
      updateSlideNumber();
      galApi.prevSlide();
    }
  }

  function handleGoTo(slideCounterNumber) {
    pauseVimeo();
    currentSlideCounter = slideCounterNumber;
    if (slideCounterNumber == 2) {
      updateSlideNumber();
      showInfo(function () {

      });
    } else if (slideCounterNumber > 2) {
      slideCounterNumber -= 1;
      updateSlideNumber();
      if (infoShown) {
        hideInfo();
      }
      galApi.goTo(slideCounterNumber);
    } else {
      if (infoShown) {
        hideInfo();
      }
      updateSlideNumber();
      galApi.goTo(slideCounterNumber);
    }
  }

  function updateHashValue() {
    $.address.value('work/'+ $.gallery.galVars.options.client_name +'/gallery/' + currentSlideCounter);
  }

  /**
   * Pauses the video when changing slide
   */
  function pauseVimeo() {
    if (galVars && galVars.current_slide) {
      var currentActiveSlideEl = $('#gallery li:eq(' + galVars.current_slide + ')');
      var ifr = currentActiveSlideEl.find('iframe');
      if (ifr.length > 0) {
        $f(ifr[0]).addEvent('ready', function (playerId) {
          var froogaloop = $f(playerId);
          froogaloop.api('pause');
        });
      }
    }
  }

  galTheme = {


    /* Initial Placement
     ----------------------------*/
    _init:function () {

      // Center Slide Links
      if (galApi.options.slide_links) $(galVars.slide_list).css('margin-left', -$(galVars.slide_list).width() / 2);

      // Start progressbar if autoplay enabled
      if (galApi.options.autoplay) {
        if (galApi.options.progress_bar) galTheme.progressBar();
      } else {
        if ($(galVars.play_button).attr('src')) $(galVars.play_button).attr("src", galVars.image_path + "play.png");	// If pause play button is image, swap src
        if (galApi.options.progress_bar) $(galVars.progress_bar).stop().animate({left:-$(window).width()}, 0);	//  Place progress bar
      }

      $('#projectInformation').hide();

      currentSlideCounter = galVars.current_slide + 1;

      if (galVars.slide_current.length) {
        $(galVars.slide_current).html(currentSlideCounter);
      }
      // Display total slides
      if ($(galVars.slide_total).length) {
        $(galVars.slide_total).html(galApi.options.slides.length + 1);
      }

      /* Navigation Items
       ----------------------------*/
      $(galVars.next_slide).click(function () {
        handleNext();
      });

      $(galVars.prev_slide).click(function () {
        handlePrevious();
      });

      $("img.arrowNav").hover(
         function () {
           this.src = this.src.replace("-off", "-on");
         },
         function () {
           this.src = this.src.replace("-on", "-off");
         });

      //event clean to make sure only one event handler
      $(".leftArrow").off('click');
      $(".rightArrow").off('click');

      $(".leftArrow").click(function () {
        handlePrevious();
      });

      $(".rightArrow").click(function () {
        handleNext();
      });


      // Full Opacity on Hover
      if (jQuery.support.opacity) {
        $(galVars.prev_slide + ',' + galVars.next_slide).mouseover(
           function () {
             $(this).stop().animate({opacity:1}, 100);
           }).mouseout(function () {
             $(this).stop().animate({opacity:0.6}, 100);
           });
      }

      $(galVars.play_button).click(function () {
        galApi.playToggle();
      });

      /* Window Resize
       ----------------------------*/
      $(window).resize(function () {

        // Delay progress bar on resize
        if (galApi.options.progress_bar && !galVars.in_animation) {
          if (galVars.slideshow_interval) clearInterval(galVars.slideshow_interval);
          if (galApi.options.slides.length - 1 > 0) clearInterval(galVars.slideshow_interval);

          $(galVars.progress_bar).stop().animate({left:-$(window).width()}, 0);

          if (!galVars.progressDelay && galApi.options.slideshow) {
            // Delay slideshow from resuming so Chrome can refocus images
            galVars.progressDelay = setTimeout(function () {
              if (!galVars.is_paused) {
                galTheme.progressBar();
                galVars.slideshow_interval = setInterval(galApi.nextSlide, galApi.options.slide_interval);
              }
              galVars.progressDelay = false;
            }, 1000);
          }
        }

        // Thumb Links
        if (galApi.options.thumb_links && galVars.thumb_tray.length) {
          // Update Thumb Interval & Page
          galVars.thumb_page = 0;
          galVars.thumb_interval = Math.floor($(galVars.thumb_tray).width() / $('> li', galVars.thumb_list).outerWidth(true)) * $('> li', galVars.thumb_list).outerWidth(true);

          // Adjust thumbnail markers
          if ($(galVars.thumb_list).width() > $(galVars.thumb_tray).width()) {
            $(galVars.thumb_back + ',' + galVars.thumb_forward).fadeIn('fast');
            $(galVars.thumb_list).stop().animate({'left':0}, 200);
          } else {
            $(galVars.thumb_back + ',' + galVars.thumb_forward).fadeOut('fast');
          }

        }
      });

    },

    handleGoTo: function(slideNumber, callback) {
      handleGoTo(slideNumber, callback);
    },


    /* Go To Slide
     ----------------------------*/
    goTo:function () {
      if (galApi.options.progress_bar && !galVars.is_paused) {
        $(galVars.progress_bar).stop().animate({left:-$(window).width()}, 0);
        galTheme.progressBar();
      }
    },

    /* Play & Pause Toggle
     ----------------------------*/
    playToggle:function (state) {

      if (state == 'play') {
        // If image, swap to pause
        if ($(galVars.play_button).attr('src')) $(galVars.play_button).attr("src", galVars.image_path + "pause.png");
        if (galApi.options.progress_bar && !galVars.is_paused) galTheme.progressBar();
      } else if (state == 'pause') {
        // If image, swap to play
        if ($(galVars.play_button).attr('src')) $(galVars.play_button).attr("src", galVars.image_path + "play.png");
        if (galApi.options.progress_bar && galVars.is_paused)$(galVars.progress_bar).stop().animate({left:-$(window).width()}, 0);
      }

    },


    /* Before Slide Transition
     ----------------------------*/
    beforeAnimation:function (direction) {
      if (galApi.options.progress_bar && !galVars.is_paused) $(galVars.progress_bar).stop().animate({left:-$(window).width()}, 0);

      /* Update Fields
       ----------------------------*/
      // Update slide caption
      if ($(galVars.slide_caption).length) {
        if (galApi.getField('caption')) {
          $(galVars.slide_caption).html(galApi.getField('caption'));
          $(galVars.slide_caption).stop().animate({opacity:'1'}, 400);
        }
        else {
          $(galVars.slide_caption).html('');
          $(galVars.slide_caption).stop().animate({opacity:'0'}, 400);
        }
      }

      if ($(galVars.slide_title).length) {
        if (galApi.getField('title')) {
          $(galVars.slide_title).html(galApi.getField('title'));
        }
        else {
          $(galVars.slide_title).html('');
        }
      }
    },

    /* After Slide Transition
     ----------------------------*/
    afterAnimation:function () {
      if (galApi.options.progress_bar && !galVars.is_paused) galTheme.progressBar();	//  Start progress bar
    },

    /* Progress Bar
     ----------------------------*/
    progressBar:function () {
      $(galVars.progress_bar).stop().animate({left:-$(window).width()}, 0).animate({ left:0 }, galApi.options.slide_interval);
    },
    prevSlide:function () {
      handlePrevious();
    },
    nextSlide:function () {
      handleNext();
    },
    pauseVimeo: function() {
      pauseVimeo();
    }
  };


  /* Theme Specific Variables
   ----------------------------*/
  $.gallery.themeVars = {

    // Internal Variables
    progress_delay:false, // Delay after resize before resuming slideshow
    image_path:'img/', // Default image path

    // General Elements
    play_button:'#pauseplay', // Play/Pause button
    next_slide:'#nextslide', // Next slide button
    prev_slide:'#prevslide', // Prev slide button

    slide_caption:'.caption', // Slide caption
    slide_title:'.brand-presentation', // Slide title
    slide_current:'.count', // Current slide number
    slide_total:'.counted', // Total Slides
    slide_list:'#slide-list', // Slide jump list

    progress_bar:'#progress-bar',
    projectInfo: null
  };

  /* Theme Specific Options
   ----------------------------*/
  $.gallery.themeOptions = {
    progress_bar:1, // Timer for each slide
  };

})(jQuery);