define([
], function () {
  var maskHeight = 400;
  var uiHeight = 220;
  var lineHeight = 38;
  var maskWidth = 0;
  var newHTML = "";
  var maxCount = 0;
  var scrollAmount = 470;

  return {
    init:function () {
      $.log('project Info init');

      //bind event handler only once

      $('#detail_prev').off('click');
      $('#detail_next').off('click');

      $('#detail_prev').click(function (event) {
        event.preventDefault();
        toTop();
      });


      $('#detail_next').click(function (event) {
        event.preventDefault();

        var goalY = parseInt($('#projectDetail').css('top')) - scrollAmount;
        var totalHeight = parseInt($('#projectDetail').css('height'));

        var maxScroll = (maskHeight - totalHeight);
        //alert(totalHeight);
        if (goalY < maxScroll) {
          goalY = maxScroll;
        }
        //alert($('.information-content').css('height'));
        //alert('goalY:' + goalY);
        $('.information-content').stop(true, true).animate({top:goalY});
      });

      $(window).resize(function () {

        setMaskHeight();
        updateInterface();
      });

    },
    resize:function () {
      setMaskHeight();
      updateInterface();
    }
  };

  function toTop() {
    //alert('toTop called');
    $('html, #projectDetail').animate({top:0}, 'slow');
  }


  function setMaskHeight() {
    maskHeight = $(window).innerHeight() - uiHeight;

    var numLines = countLines();
    var tempCount = maskHeight / lineHeight;

    maskHeight = lineHeight * Math.floor(tempCount) - 1;

    $('.information-wrapper').css('height', maskHeight + 'px');
  }


  function countLines() {
    var divHeight = parseInt($('.information-content').css('height'));
    lineHeight = parseInt($('.information-page p').css('line-height'));

    var lines = Math.floor(divHeight / lineHeight) - 1; //1 is an insurance
    return lines;
  }


  function updateText() {
    //alert('updateText called');

    $('#projectInformation').html(newHTML);
  }

  function updateInterface() {
    var totalHeight = parseInt($('.information-content').css('height'));

    //maskWidth = $('.information-wrapper').outerWidth();
    //alert("maskWidth: " + maskWidth);


    if (maskHeight > totalHeight) {
      $('#detail_prev').hide();
      $('#detail_next').hide();
      //$('#detail_prev').css('opacity', 0.5);
      //$('#detail_next').css('opacity', 0.5);
      $('.information-content').css({'top':'0'});
    }
    else {
      $('#detail_prev').show();
      $('#detail_next').show();
      $('#detail_prev').css('opacity', 1);
      $('#detail_next').css('opacity', 1);
    }
  }

});
