define([
   ],
   function () {
     var isNavOpen = true;
     var isShareOpen = false;

     return {
       init:function () {
         //event handler only once

         $('#expandIconNike').off('click');

         $('#expandIconNike').click(function () {
           toggleMenu();
         });

         $("#keyIcon").hover(
            function () {
              this.src = this.src.replace("_off", "_on");
            },
            function () {
              this.src = this.src.replace("_on", "_off");
            });

         $("#keyboard").hover(showKeyHelp, hideKeyHelp);

         setWidth();
         $(window).resize(function () {
           setWidth();
         });

         $('.share a').click(function (e) {
           e.preventDefault();
           toggleShare();
         });
       }
     };

     function setWidth() {

       var width = $(window).innerWidth();
       var height = $(window).innerHeight();
       $('.navigate').css({
         top:parseInt((height / 2) - ($('.navigate').height() / 1.5)) + 'px',
         left:parseInt((width / 2) - $('.navigate').width() / 1.5) + 'px'
       })

       $('#share-box').css({
         top:parseInt((height / 2) - ($('#share-box').height() / 0.8)) + 'px',
         left:parseInt((width / 2) - $('#share-box').width() / 1.9) + 'px'
       })

     }

     function showKeyHelp() {
       hideShare();
       $('.navigation-active').show();
       $('.navigation-active').stop()
          .animate({
            opacity:'1'
          }, 300);
     }

     function hideKeyHelp() {

       $('.navigation-active').stop()
          .animate({
            opacity:'0'
          }, 300, function () {
            $('.navigation-active').hide();
          });

     }

     function showShare() {

       isShareOpen = true;
       $('#share-box').show();
       $('#share-box').stop()
          .animate({
            opacity:'1'
          }, 300);
     }

     function hideShare() {
       isShareOpen = false;
       $('#share-box').stop()
          .animate({
            opacity:'0'
          }, 300, function () {
            $('.social').hide();
          });

     }

     function expandNav() {
       isNavOpen = true;
       $('#expandIconNike').text("X");
       $('#expandIconNike').removeClass("collapse-close");
       $('#expandIconNike').addClass("collapse");

       $('.project-navigation').stop(true, true).animate({bottom:'0px'});
     }

     function closeNav() {
       isNavOpen = false;
       $('#expandIconNike').text("+");

       $('#expandIconNike').addClass("collapse-close");
       $('#expandIconNike').removeClass("collapse");
       $('.project-navigation').stop(true, true).animate({bottom:'-72px'});
     }

     function toggleMenu() {
       if (isNavOpen) {
         closeNav();
       }
       else {
         expandNav();
       }
     }

     function toggleShare() {

       if (isShareOpen) {
         hideShare();
       }
       else {
         showShare();
       }
     }

   });
