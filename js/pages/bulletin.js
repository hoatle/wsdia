﻿define([
  'text!html/bulletin.html'
],
   function (template) {
     return {
       name:'bulletin',
       constructor:function (container) {

         var maskHeight = 480;
         var maskWidth = 840;
         var minItemsPerRow = 5;
         var maxItemsPerRow = 8;
         var uiHeight = 180;

         function toTop() {
           container.find('html, #bulletinList').animate({top:0}, 'slow');
         }

         function setMaskWidth() {

           var fullWidth = $(window).innerWidth() - 40;
           var itemWidth = parseInt(container.find('.bulletin-page ul li').css('width'));
           var padding = 20;
           var itemsPerRow = Math.floor(Math.max(minItemsPerRow, (fullWidth / (itemWidth + padding))));

           itemsPerRow = Math.min(itemsPerRow, maxItemsPerRow);
           maskWidth = itemsPerRow * (itemWidth + padding);
           container.find('.bulletin-page').css('width', maskWidth + 'px');
         }

         function setMaskHeight() {
           maskHeight = $(window).innerHTML() - uiHeight;
           container.find('.bulletin-page').css('height', maskHeight + 'px');
         }

         function updateInterface() {
           var totalHeight = parseInt($('.bulletinList').css('height'));

           if (maskHeight > totalHeight) {
             //$('#tj_prev').hide();
             $('#tj_next_bulletin').hide();
             $('#tj_previous_bulletin').css('opacity', 0.5);
             $('#tj_next_bulletin').css('opacity', 0.5);
           }
           else {
             //$('#tj_prev').show();
             $('#tj_next_bulletin').show();
             $('#tj_previous_bulletin').css('opacity', 1);
             $('#tj_next_bulletin').css('opacity', 1);
           }
         }

         function scrollNext() {
           var goalY = parseInt($('#bulletinList').css('top')) - 470;
           var totalHeight = parseInt($('#bulletinList').css('height'));

           var maxScroll = (maskHeight - totalHeight);

           if (goalY < maxScroll) {
             goalY = maxScroll;
           }

           $('#bulletinList').stop(true, true).animate({top:goalY});
         }

         return {
           beforeShow:function () {
             if (!container.html()) {
               container.html(template);

               container.find('#tj_previous_bulletin').click(function (event) {
                 event.preventDefault();
                 toTop();
               });

               $(window).resize(function () {
                 setMaskWidth();
                 //setMaskHeight();
                 updateInterface();
               });


               container.find('#tj_next_bulletin').click(function (event) {
                 event.preventDefault();
                 scrollNext();
               });
             }
           },
           afterShow: function() {
             setMaskWidth();
             //setMaskHeight();
             updateInterface();
           },
           container:container
         }
       }
     };
   }
);