define([
  'common/project',
  'common/projectInfo',
  'common/froogaloop2.min',
  'common/supersized.shutter'
],
   function (project, projectInfo) {
     return {
       name:'gallery',
       constructor:function (container) {
         $.log('gallery constructor called with container: ', container);
         var clientName, startSlide;
         $.gallery.themeVars['projectInfo'] = projectInfo;
         return {
           beforeShow:function (params) {
             clientName = params.clientName;
             startSlide = parseInt(params.startSlide);
             $('#gallery').empty();
             $('#projectInformation').hide();

             getPageTemplate(clientName, function (foundTemplate) {
               container.html(foundTemplate);
               slideShow();
             });
           },
           afterShow: function() {
             //display the gallery page when changing between 2 gallery by keyboard navigation
             //$('.gallery-page-container')[0].display = '';

             //work around
             //$('#gallery').show();
             galTheme.handleGoTo(startSlide);
           },
           beforeHide: function() {
             galTheme.pauseVimeo();
             $('#gallery').hide();
           },
           afterHide:function () {
             $.log('gallery after hide called');
             //$('.gallery-page-container')[0].display = '';
           },
           container:container
         }


         function slideShow() {
           var dataUrl = 'data/' + clientName + '/project.js';
           $.ajax({
             url:dataUrl,
             dataType:'script',
             success:function (data) {
               init(slideArray);
               //$('.gallery-page-container')[0].display = '';
             }
           });

           function init(slides) {
             $.gallery({
               start_slide:startSlide,
               slide_interval:7000,
               transition:6,
               transition_speed:700,
               autoplay:0,
               slide_links:'blank', // Individual links for each slide (Options: false, 'num', 'name', 'blank')
               slides:slides,
               client_name: clientName
             }, function() {
               //finish callback
               project.init();
               projectInfo.init();
             });
           }

         }

         function getPageTemplate(clientName, callback) {
           //TODO need caching?
           require(['text!html/' + clientName + '.html'], callback);
         }
       }
     }

   }
);