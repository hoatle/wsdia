define([
  'text!html/contact.html'
],
   function (template) {
     return {
       name:'contact',
       constructor:function (container) {
         var messageDelay = 2000;

         function init() {
           $('#newsletter').submit(submitForm);
           $('#sendingMessage').hide();
           $('#successMessage').hide();
           $('#failureMessage').hide();
           $('#incompleteMessage').hide();
         }

         function submitForm() {
           var newsletter = $(this);

           if (!$('#senderName').val() || !$('#senderEmail').val()) {

             $('#incompleteMessage').fadeIn().delay(messageDelay).fadeOut();

           }
           else {

             $('#sendingMessage').fadeIn();

             $.ajax({
               url:newsletter.attr('action') + "?ajax=true",
               type:newsletter.attr('method'),
               data:newsletter.serialize(),
               success:submitFinished
             });
           }

           return false;
         }

         function submitFinished(response) {
           response = $.trim(response);
           $('#sendingMessage').fadeOut();

           if (response == "success") {

             $('#successMessage').fadeIn().delay(messageDelay).fadeOut();
             $('#senderName').val("");
             $('#senderEmail').val("");
             $('#message').val("");

             //$('#hiddenDiv').delay(messageDelay+500).fadeTo( 'slow', 1 );

           } else {

             $('#failureMessage').fadeIn().delay(messageDelay).fadeOut();

           }
         }

         return {
           beforeShow:function () {
             if (!container.html()) {
               container.html(template);
               $(init);
             }
           },
           container:container
         }
       }
     };
   }
);
