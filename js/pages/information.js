define([
  'text!html/information.html'
],
   function (template) {
     return {
       name:'information',
       constructor:function (container) {

         var maskHeight = 480;
         var uiHeight = 150;

         function toTop() {
           //alert('toTop called');
           $('html, .information-content').animate({top:0}, 'slow');
         }

         function setMaskHeight() {
           //alert('setMaskHeight called');
           maskHeight = $(window).innerHeight() - uiHeight;
           $('.information-wrapper').css('height', maskHeight + 'px')
         }

         function updateInterface() {
           var totalHeight = parseInt($('.information-content').css('height'));
           if (maskHeight > totalHeight) {
             //$('#tj_prev').hide();
             $('#tj_next_info').hide();
             $('#tj_prev_info').css('opacity', 0.5);
             $('#tj_next_info').css('opacity', 0.5);
           }
           else {
             //$('#tj_prev').show();
             $('#tj_next_info').show();
             $('#tj_prev_info').css('opacity', 1);
             $('#tj_next_info').css('opacity', 1);
           }
         }

         return {
           beforeShow:function () {
             if (!container.html()) {
               container.html(template);

               container.find('#tj_prev_info').click(function (event) {
                 event.preventDefault();
                 toTop();
               });

               $(window).resize(function () {
                 setMaskHeight();
                 updateInterface();
               });


               container.find('#tj_next_info').click(function (event) {
                 event.preventDefault();
                 var goalY = parseInt($('.information-content').css('top')) - 470;
                 var totalHeight = parseInt($('.information-content').css('height'));

                 var maxScroll = (maskHeight - totalHeight);
                 //alert(totalHeight);
                 if (goalY < maxScroll) {
                   goalY = maxScroll;
                 }
                 //alert($('.information-content').css('height'));
                 //alert('goalY:' + goalY);
                 $('.information-content').stop(true, true).animate({top:goalY});
               });
             }
           },
           afterShow: function() {
             setMaskHeight();
             updateInterface();
           },
           container:container
         }
       }
     };
   }
);