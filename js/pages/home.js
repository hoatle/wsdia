﻿define([
  'text!html/home.html'
],
   function (template) {
     return {
       name:'home',
       constructor:function (container) {
         var os = String(BrowserDetect.OS).toLowerCase();
         var dataURL = "data/home.js";
         var isMenuOpen = true;  //keep track of whether or not the menu is collapsed
         var config = {
           over:mouseOn,
           timeout:400, // number = milliseconds delay before onMouseOut
           sensitivity:7,
           out:mouseOff
         };

         function setWidth() {
           var width = $(window).innerWidth();
           var height = $(window).innerHeight();
           container.find('.navigate').css({
             top:parseInt((height / 2) - (container.find('.navigate').height() / 1.5)) + 'px',
             left:parseInt((width / 2) - container.find('.navigate').width() / 1.5) + 'px'
           })
         }

         function mouseOn() {
           if (!isMenuOpen) {
             return;
           }
           container.find('.navigation').css('display', 'block');
           container.find(this).stop(true, true).animate({
             height:'+=22px'
           });
         }

         function mouseOff() {
           if (!isMenuOpen) {
             return;
           }
           container.find('.navigation').css('display', 'none');
           container.find(this).stop(true, true).animate({
             height:'-=22px'
           });
         }

         $(window).smartresize(function () {
           setWidth();
         });

         function expandMenu() {
           isMenuOpen = true;
           $('#expandItem').text("X");
           $('#expandItem').removeClass("collapse-close");
           $('#expandItem').addClass("collapse");

           $('.rollover').stop(true, true).animate({bottom:'0px'});
         }

         function closeMenu() {
           isMenuOpen = false;
           $('#expandItem').text("+");

           $('#expandItem').addClass("collapse-close");
           $('#expandItem').removeClass("collapse");
           $('.rollover').stop(true, true).animate({bottom:'-72px'});
         }

         function toggleMenu() {
           //alert('toggleMenu');
           //alert(isMenuOpen);
           if (isMenuOpen) {
             closeMenu();
           }
           else {
             expandMenu();
           }
         }


         return {
           beforeShow:function () {
             if (!container.html()) {
               container.html(template);
               //container.find('.rollover').hoverIntent(config);

               if (os == "iphone" || os == "ipod" || os == "ipad") {
                 //mouseOn();
                 container.find('.navigation').css({'display':'block'});
                 container.find('.rollover').css({'height':'67px'});
               }
               else {
                 container.find('.navigation').css('display', 'none');
                 container.find('.rollover').hoverIntent(config);
               }

               container.find('#expandItem').click(function () {
                 toggleMenu();
               });

               setWidth();

               this.container = container.add($('#supersized'));
             }

             $.ajax({
               url:dataURL,
               dataType:'script',
               success:function (data) {
                 //alert('data: ' + data);

                 $('#supersized').empty();
                 $.supersized({
                   slideshow:1, //Slideshow on/off
                   autoplay:1, //Slideshow starts playing automatically
                   start_slide:0, //Start slide (0 is random)
                   random:1, //Randomize slide order (Ignores start slide)
                   slide_interval:7000, //Length between transitions
                   transition:6, //0-None, 1-Fade, 2-Slide Top, 3-Slide Right, 4-Slide Bottom, 5-Slide Left, 6-Carousel Right, 7-Carousel Left
                   transition_speed:700, //Speed of transition
                   new_window:1, //Image links open in new window/tab
                   pause_hover:0, //Pause slideshow on hover
                   keyboard_nav:1, //Keyboard navigation on/off
                   performance:2, //0-Normal, 1-Hybrid speed/quality, 2-Optimizes image quality, 3-Optimizes transition speed // (Only works for Firefox/IE, not Webkit)
                   image_protect:1, //Disables image dragging and right click with Javascript
                   image_path:'img/', //Default image path

                   //Size & Position
                   min_width:0, //Min width allowed (in pixels)
                   min_height:0, //Min height allowed (in pixels)
                   vertical_center:1, //Vertically center background
                   horizontal_center:1, //Horizontally center background
                   fit_portrait:1, //Portrait images will not exceed browser height
                   fit_landscape:0, //Landscape images will not exceed browser width

                   //Components
                   navigation:1, //Slideshow controls on/off
                   thumbnail_navigation:1, //Thumbnail navigation
                   slide_counter:1, //Display slide numbers
                   slide_captions:1, //Slide caption (Pull from "title" in slides array)
                   slides:_slideArray
                 });

               }
             });
           },
           afterHide:function () {
             api.playToggle();
           },
           container:container
         }
       }
     }
   }
);