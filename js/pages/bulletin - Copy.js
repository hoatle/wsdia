﻿define([
		'text!html/bulletin.html'
	],
	function(template) {
		return {
			name: 'bulletin',
			constructor: function(container) {
                return {
					beforeShow: function() {
						if (!container.html())
                            container.html(template);
                    },
                    container: container
				}
			}
		};
	}
);