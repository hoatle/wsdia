define([
		'text!html/project.html'      
	],
	function(template, listTemplate) {
		return {
			name: 'project',
			constructor: function(container) {
               
                function render() {
                    container
                        .html(_.template(template, { data: data }))
                }

                return {
					beforeShow: function() {
						if (!data) {
                            $.ajax({
					            url: 'data/project.xml',
					            dataType: 'xml',
                                success: function(xml) {
                                    data = $(xml);
                                    render();
                                }
				            });
                        } else {
                            render();
                        }
                    },
                    container: container
				}
			}
		};
	}
);