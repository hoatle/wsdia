define([
		'text!html/work.html',
        'text!html/workItems.html'
	],
	function(template, listTemplate) {
		return {
			name: 'work',
			constructor: function(container) {
                var headerMaxHeight = 238;
var maskHeight = 480;
var maskWidth = 840;
var minItemsPerRow = 5;
var maxItemsPerRow = 8;
var uiHeight = 180;				
                var data, emptyCategory = -1;
                var serviceMenuOpen = false;				
				var isMenuOpen= false;
				var _serviceHTML;
				
                function getListWidth() {
                    var fullWidth = container.find('.work-page').innerWidth();
                    var itemWidth = container.find('.tj_gallery li:visible').outerWidth(true);

                    var itemsPerRow = Math.min(Math.max(Math.floor(fullWidth / itemWidth), minItemsPerRow), maxItemsPerRow);

                    return itemsPerRow * itemWidth;
                }

				function updateWidth(){
					container.find('#tj_container').width(getListWidth());
				}
				
                function getRowsCount() {
                    var fullHeight = container.height();
                    var itemHeight = container.find('.tj_gallery li:visible').outerHeight(true);
                    var footerHeight = container.find('footer').outerHeight() + parseInt(container.find('footer').css('bottom'));
                    var topIndent = parseInt(container.find('.work-page').css('paddingTop'))

                    var maxRowsCount = 3;
                    var rowsCount = Math.min(Math.floor((fullHeight - topIndent - headerMaxHeight - footerHeight) / itemHeight), maxRowsCount);

                    return rowsCount;
                }
				
				function openFilterMenu(){
					isMenuOpen = true;
					$('header').animate({
						height: '228px'
					});
					$('.service-list').css('display','block');
					$('.sort-by-service').addClass("service-active");
				}
				
				function closeFilterMenu(){
					isMenuOpen = false;
					$('header').animate({
						height: '28px'
					});
					$('.service-list').css('display','none');	
					$('.sort-by-service').removeClass("service-active");
				}

				function initThumbBtns(){
					//alert("initThumbBtns called");
                        container.find('.work-page .portfolio li a').hover(function(){
                            offset = $(this).offset();
                            var topPosition;
							var startPosition;
							
							if(isMenuOpen) {
								if (offset.top  <= 293){
									topPosition = '200px';
									startPosition = '210px';
								}
								else{
									topPosition = '-200px';
									startPosition = '-190px';
								} 
							}
							else {
								if (offset.top  == 93){
									topPosition = '200px';
									startPosition = '210px';
								}
								else{
									topPosition = '-200px';
									startPosition = '-190px';
								} 
							}
							
							var projectHover = $($(this).next('div'));
							
							projectHover.css({
											'z-index': '1',
											left: -(offset.left-30)+'px',
											top: topPosition//startPosition
										});
			
							projectHover.show();
							//projectHover.delay(200).animate({'opacity': 1, 'top':topPosition}, 500);
							}, function(){
								$($(this).next('div')).hide();
								//$($(this).next('div')).stop(true, true).animate({'opacity': 0, 'top':startPosition}, 300).hide();
							});


						 container.find('.service-list li a').click(function(e) {
                           // e.preventDefault();

                           // var categoryId = $(this).attr('category_id');
                            //if (container.find('.service-list .current-service').attr('category_id') != categoryId)
                            //    filterList(categoryId);
                        });
						
                    
					$(".portfolio li").hover(function(){ 					
						fadeAll($(this));
						}, function(){fadeReset();}
					);				
				
				}
				
				function scrollNext(){
					var goalY = parseInt($('.tj_wrapper').css('top')) - 470;
					var totalHeight = parseInt($('.tj_wrapper').css('height'));
					
					var maxScroll = (maskHeight - totalHeight);
					//alert(totalHeight);
					if (goalY < maxScroll){
						goalY = maxScroll;
					}
					//alert($('.information-content').css('height'));
					//alert('goalY:' + goalY);
					$('#bulletinList').stop(true,true).animate({top:goalY});
				}
				
				 function toTop(){
					  //alert('toTop called');
						  $('html, .tj_wrapper').animate({top:0}, 'slow');
				 }
				
                function renderList(projects, skipAnimation) {
                    container.find('.project-items')
                        .show()
                        .html(_.template(listTemplate, { projects: projects }));

					$('#tj_prev').click(function(e){
						e.preventDefault();
						toTop();
					});
					$('#tj_next').click(function(e){
						e.preventDefault();
						scrollNext();
					});
						
                    initThumbBtns();
                }
				
				function filterPage($params){
					
					if ($params != null){
						if ($params.func == "filter"){										
							filterList($params.category);
						}
						else{
							filterList(emptyCategory);
						}
					}
					else{
						filterList(emptyCategory);
					}
				
				}

                function filterList(categoryId) {
					//alert('filterList: ' + categoryId);
                    var projects = data.find('work > projects > project').filter(function() {
                        return categoryId == emptyCategory || !!_.detect($(this).find('categories > cat'), function(cat) { return $(cat).text() == categoryId; });
                    })
					var count = projects.length;

					var countString = "";
					if (count < 10){
						countString = "0" + count;
					}
					else{
						countString = count;
					}
					
					container.find('.count').text(countString);
					
                    container.find('.service-list li a').removeClass('current-service');
                    container.find('.service-list li a[category_id=' + categoryId + ']').addClass('current-service');

                    container.find('.tj_nav').html(container.find('.tj_nav').html());
                    container.find('#tj_prev').click(function() { setupListNavKinks(-1); });
                    container.find('#tj_next').click(function() { setupListNavKinks(1); });

                    var skipAnimation = $.browser.msie && $.browser.version <= 8;
                    if (skipAnimation || !container.find('#tj_container').html()) {
                        renderList(projects, skipAnimation);
                    } else {
                        container.find('.project-items').fadeOut('fast', function(){ renderList(projects); });
                    }
                }

                function setupListNavKinks(direction) {
                    var wrapper = container.find('.tj_gallery');
                    if (wrapper.data('anim'))
                        return;

                    var config = wrapper.data('config');
                    var totalItems = wrapper.find('li').length;
                    var showNavLinks = config.shownItems < totalItems;

                   // container.find('#tj_prev').toggle(showNavLinks && (config.currentRow + direction) > 1);
					if (showNavLinks && (config.currentRow + direction) > 1){
						container.find('#tj_prev').animate({opacity: "1"}, 300);
					}
					else{
						container.find('#tj_prev').animate({opacity: "0.35"}, 300);
					}
                    //container.find('#tj_next').toggle(showNavLinks && ((config.currentRow - 1 + direction) * config.rowCount + config.shownItems) < totalItems);
					
					if (showNavLinks && ((config.currentRow - 1 + direction) * config.rowCount + config.shownItems) < totalItems){
						container.find('#tj_next').animate({opacity: "1"}, 300);
					}
					else{
						container.find('#tj_next').animate({opacity: "0.35"}, 300);
					}
                }

				function fadeAll(item){
					//alert('fadeAll called');
					var workItems = $('.portfolio li');
					var index = workItems.index(item);
					//alert(index);
					$('.portfolio li img').each(function() {
						var itemIndex = workItems.index($(this).parent().parent());
						
						if (parseInt(itemIndex) != parseInt(index)){
							//$(this).stop(true, true).fadeTo(300, 0.2); 
							$(this).css({'opacity': '0.2'});
						}
					});
				}
				
				function fadeReset(){
					$('.portfolio li img').each(function() {
							//$(this).fadeTo(300, 1); 
							$(this).stop(true, true)
							$(this).css({'opacity': '1'});
					});		
				}

                function render() {
					_serviceHTML = _.template(template, { data: data, emptyCategory: emptyCategory });
					//alert(_serviceHTML);
                    container.html(_serviceHTML);
                        container.find('.sort-by-service').click(function(e){
                            e.preventDefault();
							if (!isMenuOpen){
								openFilterMenu();
							}
							else{
								closeFilterMenu();
							}
                        });
                        container.find('.close a').click(function(e){
                            e.preventDefault();
							closeFilterMenu();
                        });
                        container.find('.reset').click(function(e) {
                           // e.preventDefault();

                           // if (container.find('.service-list .current-service').attr('category_id') != emptyCategory)
                           //     filterList(emptyCategory);
                        });	
					
					$(window).resize(function() {
						//updateWidth();
					});
                }

                return {
					beforeShow: function($params) {
					
						//alert('$params: ' + $params);
						if (!data) {
                            $.ajax({
					            url: 'data/work.xml',
					            dataType: 'xml',
                                success: function(xml) {
                                    data = $(xml);
                                    render();
									
									filterPage($params);
                                }
				            });
                        } else {
							//render();
							filterPage($params);
                        }
                    },
										
                    container: container
				}
			}
		};
	}
);