/**
 * The routing controller object.
 *
 * Usage:
 * + Define the routing table by controller.map(valueRegex, callback);
 * the callback will be called and passed the value as the first argument.
 * + Routing action by: controller.route(definedValue). If no definedValue matches, do nothing.
 */

define(
  function() {
    var mappings = [];
    return {
      /**
       * Defines mapping rules.
       *
       * @param valueRegex the javascript regular expression object
       * @param callback the callback function
       */
      map: function(valueRegex, callback) {
        if (mapRegistered(valueRegex)) {
          $.log(valueRegex + ' was already mapped, no override map supported (yet)!');
          return;
        } else {
          mappings.push({
            'key': valueRegex,
            'value': callback
          });
        }
      },
      /**
       * Routes the matched callback registered from {@link #map(String, String)}.
       *
       * @param value the value to check
       */
      route: function(value, noMatchCallback) {
        $.log('route to: ', value);
        var callback = getMatchedCallback(value);
        if (callback) {
          $.log('route matched', callback);
          callback(value);
          return;
        }
        noMatchCallback(value);
      }
    };


    //private methods
    function mapRegistered(valueRegex) {
      var matched = false;
      _.each(mappings, function(mapObj) {
        if (matched) {
          return;
        }
        if (_.isEqual(mapObj['key'], valueRegex)) {
          matched = true;
          return;
        }
      });
      return matched;
    }

    function getMatchedCallback(value) {
      var callback = null;
      _.each(mappings, function(mapObj) {
        if (callback) {
          return callback;
        }
        if (value.match(mapObj['key'])) {
          callback = mapObj['value'];
        }
      });
      return callback;
    }
  }
);